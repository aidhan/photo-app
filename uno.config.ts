import {
  defineConfig,
  presetAttributify,
  presetUno,
  presetWebFonts,
  transformerDirectives,
} from "unocss";

export default defineConfig({
  presets: [
    presetUno(),
    presetAttributify(),
    presetWebFonts({
      provider: "google",
      fonts: {
        sans: {
          name: "Source Sans 3",
          weights: [400, 500, 700],
          italic: true,
        },
        serif: {
          name: "Lora",
          weights: [400, 500, 600, 700],
          italic: true,
        },
        mono: {
          name: "JetBrains Mono",
          weights: [400, 700],
          italic: true,
        },
      },
    }),
  ],
  transformers: [transformerDirectives()],
  shortcuts: {
    logo: "text-[1.5rem] font-semibold font-serif italic tracking-tight",
    title: "text-4xl font-bold font-serif tracking-tight leading-normal",
    subtitle: "text-xl font-medium font-sans opacity-70",
    icon: "w-5 h-5 stroke-current p-[2px] mb-[2px] rounded-sm inline-block",
    contained: "container mx-auto p-4",

    // Text
    ul: "underline decoration-[0.12em] decoration-offset-[0.08em] decoration-current",
    apad: "px-[0.4em] py-[0.2em]",
    emlink: "apad ul decoration-primary hover:bg-primary hover:text-light",

    typereset: "text-dark dark:text-light font-sans font-normal",

    input:
      "border border-dark dark:border-light rounded-sm max-w-full w-64 p-2 px-3",
    btn: "border border-dark dark:border-light rounded-sm p-2 px-3 hover:bg-dark dark:hover:bg-light hover:text-light dark:hover:text-dark",
  },
  theme: {
    colors: {
      primary: {
        DEFAULT: "#1EAE99",
      },
      dark: {
        DEFAULT: "#1E2433",
      },
      light: {
        DEFAULT: "#F2F4F7",
      },
    },
  },
});
