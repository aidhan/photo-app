export default {
  nitro: {
    preset: "vercel-edge",
  },

  modules: ["@unocss/nuxt", "@nuxtjs/fontaine"],
};
